# Final Project: Cats & Dogs (v.1.0.6)

This project is taken from Kaggle: [Cat and Dog](https://www.kaggle.com/tongpython/cat-and-dog)

![dog_cat](https://miro.medium.com/max/1400/1*oB3S5yHHhvougJkPXuc8og.gif)

Here's the [link](https://bitbucket.org/mehrdust/ml_final_proj/) to project's git repository

## Contributors:
* Peihang Xu (richardxu93@gmail.com)
* Ryan Samimi (rsamimi@mehrdust.com)
* Feifan Yu (Feifan-Yu@hotmail.com)

---
## Project Files
The compressed project zip file should contain the following files and folders:
* dumps
* images
* .gitignore
* Final_project_distinguish_cat_dog.ipynb
* conda-requirements.txt
* pip-requirements.txt
* README.md

## Setup environment
If using conda, you can use `*-requirements.txt` files to setup the new environment (e.g. `final_pr`)

```
    $ conda create final_pr --file conda-requirements.txt
    $ conda activate final_pr
```

## Prepare dataset
The dataset contains 10,000 pictures of cats and dogs which are compressed into `cat-and-dog.zip` file. First download [cat-and-dog](https://drive.google.com/file/d/18ren3ZZ4ekmCoeOuGXZYSUuoyewNJb-P/view?usp=sharing) zip file and then unzip `cat-and-dog.zip` and the dataset is ready to be used. 
Make sure to unzip in the same folder as the project's `.ipynb` file.

## dumps
To save time, `*.h5` files for each model are stored in `dumps` folder. `.pkl` files can be downloaded from this [link](https://drive.google.com/file/d/1BfhHTUEr_LsjI0S4i02OZMbJug4jNEBP/view?usp=sharing)